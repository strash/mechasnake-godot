extends Node2D


class_name Food


signal was_eaten


func _ready() -> void:
	set_entity_z_index()
	$AnimationPlayer.play("sphere_show")
	yield(get_tree().create_timer(0.2), "timeout")
	$AnimationPlayer.play("sphere_idle")
	$CPUParticles2D.emitting = true


func set_entity_z_index() -> void:
	self.z_index = convert(self.position.y, TYPE_INT)


func _on_Food_area_entered(_area) -> void:
	emit_signal("was_eaten")
	$AnimationPlayer.play("sphere_eaten")
	yield(get_tree().create_timer(0.2), "timeout")
	self.queue_free()
