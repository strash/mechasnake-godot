extends Node2D


class_name KeyCap


enum BODY { HEAD, UP, DOWN, LEFT, RIGHT } # body part/direction constants


var KEY_TYPE: int
var BODY_PART: int
var tier: int
var food_count:= 0


func _ready() -> void:
	# set textures by tier
	var texture = load("res://assets/image/tier/" + str(tier) + "/" + str(BODY_PART) + ".png")
	var shadow = load("res://assets/image/tier/" + str(tier) + "/shadow.png")
	$KeyCap.texture = texture
	$Shadow.texture = shadow
	# animate head
	$AnimationPlayer.playback_speed = tier
	if BODY_PART == BODY.HEAD:
		$AnimationPlayer.play("head_idle")
	else:
		$CollisionShape2D.disabled = true
		$CollisionShape2D.queue_free()
	set_entity_z_index()


func set_entity_z_index() -> void:
	self.z_index = self.position.y as int


func animate_keypress() -> void:
	$AnimationPlayer.play("key_press")


func eat_food(max_tier: int) -> void:
	food_count += 1
	if food_count >= tier and tier < max_tier:
		food_count = 0
		tier += 1
		increase_keycap_tier(tier)


func increase_keycap_tier(next_tier: int) -> void:
	var texture = load("res://assets/image/tier/" + str(next_tier) + "/" + str(BODY_PART) + ".png")
	var shadow = load("res://assets/image/tier/" + str(next_tier) + "/shadow.png")
	$KeyCap.texture = texture
	$Shadow.texture = shadow
	if BODY_PART == BODY.HEAD:
		tier = next_tier
		$AnimationPlayer.playback_speed = next_tier
	else:
		$AnimationPlayer.playback_speed = convert(next_tier, TYPE_REAL) / 2
