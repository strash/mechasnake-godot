extends Control


onready var SCORE:Label = get_node("Score")
var score:= 0
onready var BODY = [
	{
		"Score": get_node("VBoxContainer/Head/V/Score") as Label,
		"Tier": get_node("VBoxContainer/Head/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/W/V/Score") as Label,
		"Tier": get_node("VBoxContainer/W/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/S/V/Score") as Label,
		"Tier": get_node("VBoxContainer/S/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/A/V/Score") as Label,
		"Tier": get_node("VBoxContainer/A/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/D/V/Score") as Label,
		"Tier": get_node("VBoxContainer/D/V/Tier") as Label,
	},
]
const COLOR = [
	Color8(211, 218, 224),
	Color8(173, 175, 184),
	Color8(116, 119, 128),
	Color8(47, 48, 54),
	Color8(178, 29, 74),
	Color8(41, 73, 202),
	Color8(13, 92, 33),
	Color8(206, 55, 24),
	Color8(221, 144, 1),
]


func _ready() -> void:
	var j = 0
	for i in BODY:
		if j == 0:
			i.Score.text = "1 tier"
		else:
			i.Score.text = "0/1"
		i.Tier.text = "LIGHT"
		i.Tier.set("custom_colors/font_color", COLOR[0])
		j += 1
	SCORE.text = str(score)


func _process(_delta: float) -> void:
	SCORE.text = str(score)


func set_keycap_text(data: Array) -> void:
	var j = 0
	for i in BODY:
		if j == 0:
			i.Score.text = "%s tier" % str(data[j].to)
		else:
			if data[j].to == COLOR.size():
				i.Score.text = "max"
			else:
				i.Score.text = "%s\/%s" % [str(data[j].from), str(data[j].to)]
		i.Tier.text = str(data[j].tier)
		i.Tier.set("custom_colors/font_color", COLOR[data[j].to - 1])
		j += 1


func set_score_text(increase: int) -> void:
	$Tween.interpolate_property(self, "score", score, score + increase, 0.3)
	if not $Tween.is_active():
		$Tween.start()

