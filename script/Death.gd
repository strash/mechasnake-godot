extends ColorRect


signal reload_game


var score:= 0
var is_shown = false
onready var tween:= $Tween
onready var Score:Label = $Center/Score


func _ready() -> void:
	$AnimationPlayer.stop()


func _process(_delta: float) -> void:
	Score.text = "SCORE: " + str(score)


func _input(event: InputEvent) -> void:
	if event and InputEventKey and Input.is_action_just_pressed("ui_space") and is_shown:
		emit_signal("reload_game")


func show_view(new_score: int) -> void:
	is_shown = true
	$AnimationPlayer.play("space_idle")
	tween.interpolate_property(self, "modulate:a", 0, 1, 0.8)
	if not tween.is_active():
		tween.start()
	yield(get_tree().create_timer(0.8), "timeout")
	_set_score(new_score)


func hide_view() -> void:
	score = 0
	$AnimationPlayer.stop()
	tween.interpolate_property(self, "modulate:a", 1, 0, 0.3)
	if not tween.is_active():
		tween.start()
	yield(get_tree().create_timer(0.3), "timeout")
	is_shown = false


func _set_score(new_score: int) -> void:
	tween.interpolate_property(self, "score", score, new_score, 2)
	if not tween.is_active():
		tween.start()
