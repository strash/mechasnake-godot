extends Node2D


export (PackedScene) var KeyCap


signal set_score_data
signal set_keycap_data
signal show_death_screen

enum BODY { HEAD, UP, DOWN, LEFT, RIGHT } # body part/direction constants
enum KEY_TYPE { REGULAR, MODIFIER } # key type
enum TIER { LIGHT = 1, GREY, DARKGREY, DARK, PINK, BLUE, GREEN, ORANGE, GOLD } # tier
const TIER_MAP = {
	1: "LIGHT",
	2: "GREY",
	3: "DARK GREY",
	4: "DARK",
	5: "PINK",
	6: "BLUE",
	7: "GREEN",
	8: "ORANGE",
	9: "GOLD",
}

const GRID:= Vector2(26, 26) # grid
const CELL:= Vector2(60, 40) # grid cell size

var SNAKE:= {
	"body": [], # array of keycaps
	"speed": 100, # snake speed
	"speed_step": 150, # speed increment
	"direction": BODY.RIGHT, # starting direction
	"position": Vector2(570, 324), # starting position
	"length": 5, # starting length
}

const START_TIER = 1


# snake head position and direction
func set_snake_direction(direction: int, duration: float) -> void:
	var head = SNAKE.body[0]
	var new_pos = head.position
	if SNAKE.direction == BODY.RIGHT:
		if direction == BODY.LEFT or direction == BODY.UP:
			new_pos.y = new_pos.y - CELL.y
			SNAKE.direction = BODY.UP
		elif direction == BODY.DOWN:
			new_pos.y = new_pos.y + CELL.y
			SNAKE.direction = direction
		else:
			new_pos.x = new_pos.x + CELL.x
	elif SNAKE.direction == BODY.LEFT:
		if direction == BODY.RIGHT or direction == BODY.UP:
			new_pos.y = new_pos.y - CELL.y
			SNAKE.direction = BODY.UP
		elif direction == BODY.DOWN:
			new_pos.y = new_pos.y + CELL.y
			SNAKE.direction = direction
		else:
			new_pos.x = new_pos.x - CELL.x
	elif SNAKE.direction == BODY.UP:
		if direction == BODY.RIGHT or direction == BODY.DOWN:
			new_pos.x = new_pos.x + CELL.x
			SNAKE.direction = BODY.RIGHT
		elif direction == BODY.LEFT:
			new_pos.x = new_pos.x - CELL.x
			SNAKE.direction = direction
		else:
			new_pos.y = new_pos.y - CELL.y
	elif SNAKE.direction == BODY.DOWN:
		if direction == BODY.LEFT or direction == BODY.UP:
			new_pos.x = new_pos.x - CELL.x
			SNAKE.direction = BODY.LEFT
		elif direction == BODY.RIGHT:
			new_pos.x = new_pos.x + CELL.x
			SNAKE.direction = direction
		else:
			new_pos.y = new_pos.y + CELL.y
	set_snake_position(head, head.position, new_pos, duration)

# snake body part position
# assignment of a position from the previous body part
func set_snake_position(key: KeyCap, from: Vector2, to: Vector2, duration: float) -> void:
	var tween = key.get_node("Tween")
	tween.interpolate_property(key, "position", from, to, duration)
	tween.start()


func _ready() -> void:
	# factory
	for i in SNAKE.length:
		var keycap = KeyCap.instance()
		keycap.KEY_TYPE = KEY_TYPE.REGULAR
		keycap.BODY_PART = i
		keycap.tier = START_TIER
		keycap.position.x = SNAKE.position.x - CELL.x * i
		keycap.position.y = SNAKE.position.y
		self.add_child(keycap)
		SNAKE.body.append(keycap)
	emit_signal("set_keycap_data", _get_score_data())


var tick:= 0.0
var tick_max:= 100.0
var new_direction: int = BODY.RIGHT

func _process(delta: float) -> void:
	tick += delta * SNAKE.speed

	if tick >= tick_max:
		# check collisions
		if collision_wall(new_direction) or collision_self():
			death()
		else:
			# set speed
			SNAKE.speed = SNAKE.speed_step * SNAKE.body[0].tier
			# set position
			for i in range(SNAKE.length - 1, 0, -1): # iterating from tail to head
				set_snake_position(SNAKE.body[i], SNAKE.body[i].position, SNAKE.body[i - 1].position, tick_max / SNAKE.speed)
			# set direction
			set_snake_direction(new_direction, tick_max / SNAKE.speed)

		tick = 0
	# set z-index
	for i in get_tree().get_nodes_in_group("zindex_group"):
		i.set_entity_z_index()


func _input(event: InputEvent) -> void:
	if event and InputEventKey:
		if Input.is_action_pressed("ui_right"):
			new_direction = BODY.RIGHT
			_animate_keypress()
		if Input.is_action_pressed("ui_left"):
			new_direction = BODY.LEFT
			_animate_keypress()
		if Input.is_action_pressed("ui_up"):
			new_direction = BODY.UP
			_animate_keypress()
		if Input.is_action_pressed("ui_down"):
			new_direction = BODY.DOWN
			_animate_keypress()


func increase_tier() -> void:
	var total_tier: float = 0.0
	for i in SNAKE.body:
		if i.KEY_TYPE == KEY_TYPE.REGULAR:
			if i.BODY_PART == new_direction:
				i.eat_food(TIER.GOLD)
				emit_signal("set_score_data", i.tier)
			if i.BODY_PART != BODY.HEAD:
				total_tier += i.tier
	# encreasing head tier
	var head_tier = floor(total_tier / 4)
	SNAKE.body[0].increase_keycap_tier(head_tier)
	emit_signal("set_keycap_data", _get_score_data())
	$EatAudio.play()


# collision with walls
func collision_wall(direction: int) -> bool:
	var head = SNAKE.body[0].position - CELL / 2
	head.y = head.y + 16
	return (head.x <= 0.1 and direction == BODY.LEFT) or (head.y <= 0.1 and direction == BODY.UP) or (head.x + CELL.x >= GRID.x * CELL.x and direction == BODY.RIGHT) or (head.y + CELL.y >= GRID.y * CELL.y and direction == BODY.DOWN)


# self collition
func collision_self() -> bool:
	var head = SNAKE.body[0].position

	for i in range(1, SNAKE.length):
		if head.x == SNAKE.body[i].position.x and head.y == SNAKE.body[i].position.y:
			return true
	return false


# game over
func death() -> void:
	SNAKE.speed = 0
	for i in SNAKE.body:
		$Tween.interpolate_property(i, "modulate:a", 1, 0.6, 0.6, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		i.get_node("AnimationPlayer").stop()
	$Tween.start()
	emit_signal("show_death_screen")
	$DeathAudio.play()


func _animate_keypress() -> void:
	for i in SNAKE.body:
		if i.has_method("animate_keypress") and i.BODY_PART == new_direction:
			i.animate_keypress()


func _get_score_data() -> Array:
	var score_data = []
	for i in SNAKE.length:
		score_data.append({
			"from": SNAKE.body[i].food_count,
			"to": SNAKE.body[i].tier,
			"tier": TIER_MAP[SNAKE.body[i].tier]
		})
	return score_data
