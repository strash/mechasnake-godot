extends Node


onready var FoodScene = preload("res://scene/Food.tscn")


enum BODY { HEAD, UP, DOWN, LEFT, RIGHT } # body part/direction constants


const GRID:= Vector2(26, 26) # grid
const CELL:= Vector2(60, 40) # grid cell size

var overlay_alpha: float = 0.0
var self_paused = false
var death = false
var sound = true


func _ready() -> void:
	spawn_food()
	var _on_Snake_set_score_data = $SnakeLayer/Snake.connect("set_score_data", self, "_on_Snake_set_score_data")
	var _on_Snake_set_keycap_data = $SnakeLayer/Snake.connect("set_keycap_data", self, "_on_Snake_set_keycap_data")
	var _on_Snake_show_death_screen = $SnakeLayer/Snake.connect("show_death_screen", self, "_on_Snake_show_death_screen")
	var _on_Death_reload_game = $DeathLayer/Death.connect("reload_game", self, "_on_Death_reload_game")


func _process(_delta: float) -> void:
	# set camera position
	$Camera2D.position = $SnakeLayer/Snake.SNAKE.body[0].position
	$Camera2D.smoothing_speed = $SnakeLayer/Snake.SNAKE.body[0].tier
	if not death and not $DeathLayer/Death.is_shown:
		if InputEventKey and Input.is_action_just_pressed("ui_space"):
			if get_tree().paused:
				get_tree().paused = false
				self_paused = false
				$HUDLayer/HUD/SpaceToPause.text = "PRESS SPACE TO PAUSE"
				if sound:
					$AudioStreamPlayer.play()
			else:
				get_tree().paused = true
				self_paused = true
				$HUDLayer/HUD/SpaceToPause.text = "PRESS SPACE TO RESUME"
				$AudioStreamPlayer.stop()
		if self_paused and overlay_alpha < 1.0:
			overlay_alpha += 0.05
		elif not self_paused and overlay_alpha > 0.0:
			overlay_alpha -= 0.05
		$HUDLayer/HUD/ColorRect.modulate.a = overlay_alpha

	if InputEventKey and Input.is_action_just_pressed("ui_f"):
		if sound:
			$AudioStreamPlayer.stop()
			$HUDLayer/HUD/FToMute.text = "PRESS F TO UNMUTE"
		else:
			$AudioStreamPlayer.play()
			$HUDLayer/HUD/FToMute.text = "PRESS F TO MUTE"
		# $HUDLayer/HUD/FToMute.rect_size = Vector2.ZERO
		# $HUDLayer/HUD/FToMute.rect_position.x = 1121
		sound = not sound


func spawn_food() -> void:
	var offset = 30
	var food = FoodScene.instance()
	var grid_size = GRID * CELL

	var x = convert(rand_range(0, grid_size.x - CELL.x), TYPE_INT)
	var y = convert(rand_range(0, grid_size.y - CELL.y), TYPE_INT)
	food.position.x = x - x % convert(CELL.x, TYPE_INT) + offset
	food.position.y = y - y % convert(CELL.y, TYPE_INT)
	food.connect("was_eaten", self, "_on_Food_was_eaten")
	$SnakeLayer.call_deferred("add_child", food)


func _on_Food_was_eaten() -> void:
	spawn_food()
	$SnakeLayer/Snake.increase_tier()


func _on_Snake_set_score_data(data: int) -> void:
	$HUDLayer/HUD.set_score_text(data)


func _on_Snake_set_keycap_data(data: Array) -> void:
	$HUDLayer/HUD.set_keycap_text(data)


func _on_Snake_show_death_screen() -> void:
	death = true
	var score = $HUDLayer/HUD.score
	$DeathLayer/Death.show_view(score)


func _on_Death_reload_game() -> void:
	var snake = $SnakeLayer/Snake
	snake.SNAKE.speed = 100
	snake.SNAKE.direction = BODY.RIGHT
	snake.new_direction = BODY.RIGHT
	snake.SNAKE.position = Vector2(570, 324)
	for i in snake.SNAKE.body:
		i.queue_free()
	snake.SNAKE.body.clear()
	# snake.SNAKE = {
	# 	"body": [], # array of keycaps
	# 	"speed": 100, # snake speed
	# 	"speed_step": 150, # speed increment
	# 	"direction": BODY.RIGHT, # starting direction
	# 	"position": Vector2(570, 324), # starting position
	# 	"length": 5, # starting length
	# }
	snake._ready()
	snake.tick = 0.0

	var hud = $HUDLayer/HUD
	hud.score = 0
	hud._ready()

	$DeathLayer/Death.hide_view()

	overlay_alpha = 0.0
	self_paused = false
	death = false
